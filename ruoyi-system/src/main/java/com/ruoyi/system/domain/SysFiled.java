package com.ruoyi.system.domain;

/**
 * @ClassName SysFiled
 * @Author lmr
 * <p>
 * 扩展字段
 * <p>
 * @Date 2022/8/28 0028 15:48
 * @Version 1.0
 **/
public class SysFiled {
    private Long filedId;
    private String filedLabel;
    private String filedName;
    private String filedType;
    private String doubleLayout;
    private String requiredFiled;
    private String filedValue;

    public SysFiled() {
    }

    public SysFiled(Long filedId, String filedLabel, String filedName, String filedType, String doubleLayout, String requiredFiled) {
        this.filedId = filedId;
        this.filedLabel = filedLabel;
        this.filedName = filedName;
        this.filedType = filedType;
        this.doubleLayout = doubleLayout;
        this.requiredFiled = requiredFiled;
    }

    public String getFiledLabel() {
        return filedLabel;
    }

    public void setFiledLabel(String filedLabel) {
        this.filedLabel = filedLabel;
    }

    public String getDoubleLayout() {
        return doubleLayout;
    }

    public void setDoubleLayout(String doubleLayout) {
        this.doubleLayout = doubleLayout;
    }

    public Long getFiledId() {
        return filedId;
    }

    public void setFiledId(Long filedId) {
        this.filedId = filedId;
    }

    public String getFiledName() {
        return filedName;
    }

    public void setFiledName(String filedName) {
        this.filedName = filedName;
    }

    public String getFiledType() {
        return filedType;
    }

    public void setFiledType(String filedType) {
        this.filedType = filedType;
    }

    public String getRequiredFiled() {
        return requiredFiled;
    }

    public void setRequiredFiled(String requiredFiled) {
        this.requiredFiled = requiredFiled;
    }

    public String getFiledValue() {
        return filedValue;
    }

    public void setFiledValue(String filedValue) {
        this.filedValue = filedValue;
    }
}
