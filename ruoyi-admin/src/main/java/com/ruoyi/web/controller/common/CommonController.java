package com.ruoyi.web.controller.common;

import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.framework.config.ServerConfig;

/**
 * 通用请求处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/common")
public class CommonController
{
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private ServerConfig serverConfig;

    private static final String FILE_DELIMETER = ",";

    /**
     * 通用下载请求
     *
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @GetMapping("/download")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            if (!FileUtils.checkAllowDownload(fileName))
            {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            String filePath = RuoYiConfig.getDownloadPath() + fileName;

            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, realFileName);
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    /**
     * 通用上传请求（单个）
     */
    @PostMapping("/upload")
    public AjaxResult uploadFile(MultipartFile file) throws Exception
    {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            AjaxResult ajax = AjaxResult.success();
            ajax.put("url", url);
            ajax.put("fileName", fileName);
            ajax.put("newFileName", FileUtils.getName(fileName));
            ajax.put("originalFilename", file.getOriginalFilename());
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 通用上传请求（多个）
     */
    @PostMapping("/uploads")
    public AjaxResult uploadFiles(List<MultipartFile> files) throws Exception
    {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            List<String> urls = new ArrayList<String>();
            List<String> fileNames = new ArrayList<String>();
            List<String> newFileNames = new ArrayList<String>();
            List<String> originalFilenames = new ArrayList<String>();
            for (MultipartFile file : files)
            {
                // 上传并返回新文件名称
                String fileName = FileUploadUtils.upload(filePath, file);
                String url = serverConfig.getUrl() + fileName;
                urls.add(url);
                fileNames.add(fileName);
                newFileNames.add(FileUtils.getName(fileName));
                originalFilenames.add(file.getOriginalFilename());
            }
            AjaxResult ajax = AjaxResult.success();
            ajax.put("urls", StringUtils.join(urls, FILE_DELIMETER));
            ajax.put("fileNames", StringUtils.join(fileNames, FILE_DELIMETER));
            ajax.put("newFileNames", StringUtils.join(newFileNames, FILE_DELIMETER));
            ajax.put("originalFilenames", StringUtils.join(originalFilenames, FILE_DELIMETER));
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 本地资源通用下载
     */
    @GetMapping("/download/resource")
    public void resourceDownload(String resource, HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        try
        {
            if (!FileUtils.checkAllowDownload(resource))
            {
                throw new Exception(StringUtils.format("资源文件({})非法，不允许下载。 ", resource));
            }
            // 本地资源路径
            String localPath = RuoYiConfig.getProfile();
            // 数据库资源地址
            String downloadPath = localPath + StringUtils.substringAfter(resource, Constants.RESOURCE_PREFIX);
            // 下载名称
            String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, downloadName);
            FileUtils.writeBytes(downloadPath, response.getOutputStream());
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    @GetMapping("/test")
    public AjaxResult test() {
        ArrayList<SysUser> list1 = new ArrayList<>();
        SysUser a = new SysUser();
        a.setUserName("a");
        list1.add(a);
        SysUser b = new SysUser();
        b.setUserName("b");
        list1.add(b);
        ArrayList<SysUser> list2 = new ArrayList<>();
        SysUser c = new SysUser();
        c.setUserName("c");
        list2.add(c);
        SysUser d = new SysUser();
        d.setUserName("d");
        list2.add(d);
        list2.add(a);

        /*List<SysUser> list1_ = new ArrayList<>();
        list1_.addAll(list1);
        for (SysUser user2 : list2) {
            int i = 0;
            for (; i < list1_.size(); i++) {
                if (user2.getUserName().equals(list1_.get(i).getUserName())) {
                    // 如果存在，跳出内层循环
                    break;
                }
            }
            if (i >= list1_.size()) {
                list1.add(user2);
            }
        }*/

//        List<SysUser> different = getDifferent(list1, list2);

        // 从list2中筛选出list1中没有的元素
        /*List<SysUser> diff = list2.stream().filter(
                user2 -> !list1.stream().map(user1 -> user1.getUserName()).collect(Collectors.joining()).contains(user2.getUserName()))
                .collect(Collectors.toList());
        list1.addAll(diff);*/


//        // java中对List排序的几种方法
//        Collections.sort(hasFileds, new Comparator<SysFiled>() {
//            @Override
//            public int compare(SysFiled o1, SysFiled o2) {
//                return o1.getSeq() - o2.getSeq();
//            }
//        }.reversed());
//        Collections.sort(hasFileds, (o1, o2) -> o2.getSeq() - o1.getSeq());
//        // 升序
////        Collections.sort(hasFileds, Comparator.comparingInt(SysFiled::getSeq));
//        // 降序
////        Collections.sort(hasFileds, Comparator.comparingInt(SysFiled::getSeq).reversed());
//
//        // 使用stream排序
//        hasFileds = hasFileds.stream().sorted(Comparator.comparingInt(SysFiled::getSeq)).collect(Collectors.toList());

        return AjaxResult.success(list1);
    }


    /**
     *
     * @param list1
     * @param list2
     * @return
     */
    private static List<SysUser> getDifferent(List<SysUser> list1, List<SysUser> list2) {
        List<SysUser> diff = new ArrayList<>();
        Map<String,SysUser> map = new HashMap<>(list2.size());
        // 先将list1中的元素放入map、不同元素的集合
        for (SysUser stu : list1) {
            map.put(stu.getUserName(), stu);
            diff.add(stu);
        }
        for (SysUser stu : list2) {
            // 如果没找到list1中的元素，放入不同元素集合
            if(map.get(stu.getUserName())==null)
            {
                diff.add(stu);
            }
        }
        return diff;
    }
}
